# Noetic React Code Challenge

### Getting Started
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone https://gitlab.com/digitizelab/noetic-react.git
> cd noetic-react
> npm install
> npm start
```
Visit [localhost:8080](http://localhost:8080) after npm start.

