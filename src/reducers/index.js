import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

import PaymentReducer from './PaymentReducer'

const rootReducer = combineReducers({
    payment: PaymentReducer,
    form: formReducer
});

export default rootReducer;
