import React, {Component} from 'react';

class ProcessPayment extends Component {
    render() {
        return (
            <div>
                <div className="row card-container">
                    <div className="card-container-inner" style={{textAlign: 'center'}}>
                        <h1>Payment in process...</h1>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProcessPayment;