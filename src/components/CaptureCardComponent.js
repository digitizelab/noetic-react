import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import numeral from 'numeral';

class CaptureCard extends Component {

    renderField(field) {
        const {meta: {touched, error}} = field;
        const className = `form-group row ${touched && error ? 'has-error' : ''}`;
        const {name} = field.input;

        return (
            <div className={className}>
                <label className="col-sm-4 col-form-label">{field.label}</label>
                <div className="col-sm-8">
                    <label className="control-label" htmlFor={name}>{touched ? error : ''}</label>
                    <input
                        className="form-control"
                        type="text"
                        id={name}
                        {...field.input}
                    />
                </div>
            </div>
        );
    }

    renderSelectField(field) {
        const {meta: {touched, error}} = field;
        const className = `form-group row ${touched && error ? 'has-error' : ''}`;
        const {name} = field.input;

        return (
            <div className={className}>
                <label className="col-sm-4 col-form-label">{field.label}</label>
                <div className="col-sm-8">
                    <label className="control-label" htmlFor={name}>{touched ? error : ''}</label>
                    <select {...field.input} className="form-control" id={name}>
                        <option value="">Select Card Type</option>
                        {field.children}
                    </select>
                </div>
            </div>
        );
    }

    onSubmit = (values) => {
        //Nothing to do here, just redirecting to processing page
        this.props.history.push('/processing');
    };

    render() {
        const {payment, handleSubmit} = this.props;

        return (
            <div>
                <div className="row card-container">
                    <div className="well well-lg card-container-inner">
                        <form onSubmit={handleSubmit(this.onSubmit)}>
                            <Field
                                name="type"
                                label="Card Type"
                                component={this.renderSelectField}
                                type="select"
                            >
                                <option key="1" value="visa">Visa</option>
                                <option key="2" value="master">Master</option>
                            </Field>
                            <Field
                                label="Name on Card"
                                name="name"
                                component={this.renderField}
                            />
                            <Field
                                label="Card Number"
                                name="number"
                                component={this.renderField}
                            />
                            <Field
                                label="CVV"
                                name="cvv"
                                component={this.renderField}
                            />
                            <Field
                                label="Card Expiry"
                                name="expiry"
                                component={this.renderField}
                            />
                            <div className="form-group row">
                                <label className="col-sm-4 col-form-label">Amount(&pound;)</label>
                                <div className="col-sm-8 read-only">
                                    <p>{numeral(payment.amount).format('0,0.00')}</p>
                                </div>

                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

function validate(values) {

    const errors = {};

    if (!values.type) {
        errors.type = "Please select a card type";
    }

    if (!values.name || values.name.length < 5) {
        errors.name = "Please enter a valid name";
    }
    if (!values.number || values.number.length < 16) {
        errors.number = "Please enter valid card number";
    }
    if (!values.cvv || values.cvv.length < 3) {
        errors.cvv = "Please enter valid security code";
    }
    if (!values.expiry || values.expiry.length < 5) {
        errors.expiry = "Please enter valid expiry date";
    }

    return errors;
}

function mapStateToProps(state) {
    return {
        payment: state.payment
    };
}

export default reduxForm({
    validate,
    form: 'CaptureCardForm'
})(
    connect(mapStateToProps)(CaptureCard)
)